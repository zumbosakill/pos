Simple Point of Sale System Using PHP MySql with PDO Query Source Code

This is a simple web based application 
that will teach you on how to create 
a point of sale system using PHP MySql 
with PDO query. One of the main features 
of this program is it handles 2 type of 
payments which is cas and credit card. 
This POS also features reports for sales,inventory
and it could be done daily,weekly,and yearly.

Username: admin
Password: admin

Instructions:
1. Download the source code.
2. Extract the compressed folder.
3. Run the xampp control panel and start the apache and mysql
4. Copy the extracted folder to htdocs
5. Open the browser and type localhost/phpmyadmin
6. import the database sales
7. Run the program by typing in any browser localhost/pos
Note:

This is just a simple program any updates or constructive criticism will be
accepted.

Simple Point of Sale System Using PHP MySql with PDO Query Source Code Screenshots: