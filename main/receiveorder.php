<?php
	include('../connect.php');
	$id=$_GET['id'];
	$result = $db->prepare("UPDATE purchases set status='03' WHERE transaction_id= :memid");
	$result->bindParam(':memid', $id);
	$result->execute();

	//edit qty
	$result = $db->prepare("select * from purchases_item a LEFT JOIN purchases b on a.invoice=b.invoice_number where b.transaction_id= :memid");
	$result->bindParam(':memid', $id);
	$result->execute();
	for($i=0; $row = $result->fetch(); $i++){
		$qty=$row['qty'];
		$product_code=$row['name'];
		$sql = "UPDATE products
		        SET qty=qty+?
				WHERE product_code=?";
		$q = $db->prepare($sql);
		$q->execute(array($qty,$product_code));
	}
?>
