<html>
<head>
<title>
POS
</title>
<link href="../style.css" media="screen" rel="stylesheet" type="text/css" />
<!--sa poip up-->
<script src="argiepolicarpio.js" type="text/javascript" charset="utf-8"></script>
<script src="js/application.js" type="text/javascript" charset="utf-8"></script>
<link href="src/facebox.css" media="screen" rel="stylesheet" type="text/css" />
<script src="lib/jquery.js" type="text/javascript"></script>
<script src="src/facebox.js" type="text/javascript"></script>
<script type="text/javascript">
  jQuery(document).ready(function($) {
    $('a[rel*=facebox]').facebox({
      loadingImage : 'src/loading.gif',
      closeImage   : 'src/closelabel.png'
    })
  })
</script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
</head>
<body>
<div id="maintable">
<div style="margin-top: -19px; margin-bottom: 21px;">
<a id="addd" href="index.php" style="float: none;">Back</a>
</div>
<input type="text" name="filter" value="" id="filter" placeholder="Search Customer..." autocomplete="off" /><a rel="facebox" id="addd" href="purchases.php">Add Purchases</a><br><br>
<table id="resultTable" data-responsive="table">
	<thead>
		<tr>
			<th width="5%"> Invoice Number </th>
			<th width="21%"> Date </th>
			<th width="11%"> Supplier </th>
			<th width="11%"> Remarks </th>
			<th width="11%"> Purcash status </th>
			<th width="12%"> Action </th>
		</tr>
	</thead>
	<tbody>

			<?php
				include('../connect.php');
				$result = $db->prepare("SELECT a.transaction_id, a.invoice_number, a.date, a.suplier, a.remarks, b.status_desc, b.status_code FROM purchases a INNER JOIN master_status b ON a.status = b.status_code ORDER BY transaction_id DESC");
				$result->execute();
				for($i=0; $row = $result->fetch(); $i++){
			?>
			<tr class="record">
			<td><?php echo $row['invoice_number']; ?></td>
			<td><?php echo $row['date']; ?></td>
			<td><?php echo $row['suplier']; ?></td>
			<td><?php echo $row['remarks']; ?></td>
			<td><?php echo $row['status_desc']; ?></td>
			<td><a rel="facebox" href="view_purchases_list.php?iv=<?php echo $row['invoice_number']; ?>"> View </a> | <a href="#" id="<?php echo $row['transaction_id']; ?>" class="delbutton" title="Click To Delete">Delete</a><?php if($row['status_code'] == '02'){
				echo ' | <a href="#" id="'.$row['transaction_id'].'" class="receivepurcashbutton" title="Click To Receive Order">Receive Order</a>';
			}; ?></td>
			</tr>
			<?php
				}
			?>

	</tbody>
</table>
<div class="clearfix"></div>
</div>
<script src="js/jquery.js"></script>
  <script type="text/javascript">
$(function() {


$(".delbutton").click(function(){

//Save the link in a variable called element
var element = $(this);

//Find the id of the link that was clicked
var del_id = element.attr("id");

//Built a url to send
var info = 'id=' + del_id;
 if(confirm("Sure you want to delete this update? There is NO undo!"))
		  {

 $.ajax({
   type: "GET",
   url: "deletepppp.php",
   data: info,
   success: function(){

   }
 });
         $(this).parents(".record").animate({ backgroundColor: "#fbc7c7" }, "fast")
		.animate({ opacity: "hide" }, "slow");

 }

return false;

});

$(".receivepurcashbutton").click(function(){

//Save the link in a variable called element
var element = $(this);

//Find the id of the link that was clicked
var del_id = element.attr("id");

//Built a url to send
var info = 'id=' + del_id;
$.ajax({
  type: "GET",
  url: "receiveorder.php",
  data: info,
  success: function(){
	  location.reload();
  }
});

});

});
</script>
</body>
</html>
